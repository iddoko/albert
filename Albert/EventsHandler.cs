﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class EventsHandler
    {
        private List<Event> events;

        public EventsHandler()
        {
            events = new List<Event>();
        }

        public bool AddEvent(Event e)
        {
            events.Add(e);
            return true;
        }

        public override String ToString()
        {
            String str = "";
            if (events.Count == 0) str = "no events.";

            foreach(Event e in events)
            {
                str += e.ToString() + ",";
            }

            str.Substring(0, str.Length - 1);
            return str;
        }
    }
}
