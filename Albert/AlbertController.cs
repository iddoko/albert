﻿using System;

namespace Albert
{
    public class AlbertController
    {
        private static AlbertController instance = null;

        const string OPENING_SENTENCE = "Yes sir, what can I help you with?";
        const string DONT_UNDERSTAND = "I'm sorry, I couldn't understand what you'd like me to do";
        const string ANYTHING_ELSE = "Anything else, sir?";
        const string CREATED_EVENT = "Ok, I have just created this event for you";
        const string WOULD_YOU_LIKE = "Would you like to hear what you have this upcoming week?";

        private DateTime today = DateTime.Today;


        private AlbertController()
        {

        }
        public void startAlbert()
        {
            getInstance().Speak(OPENING_SENTENCE);
            String input = getInstance().getInput();
            LanguageParser.getInstance().parseInput(input);

        }

        public static AlbertController getInstance()
        {
            if (instance == null) instance = new AlbertController();
            return instance;
        }

        public void Speak(String sentence)
        {
            // TODO: MUST IMPLEMENT ASAP
            throw new NotImplementedException();
        }

        public String getInput()
        {
            // TODO: MUST IMPLEMENT ASAP
            throw new NotImplementedException();
        }

        public DateTime Today()
        {
            return this.today;
        }
    }
}