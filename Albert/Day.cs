﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class Day
    {
        private EventsHandler eventsHandler;
        private DayOfWeek dayOfWeek;
        private DateTime date;
        
        public Day(DateTime date)
        {
            this.date = date;
            this.dayOfWeek = date.DayOfWeek;
            this.eventsHandler = new EventsHandler();
        }

        public void addEvent(Event eventToAdd)
        {
            eventsHandler.AddEvent(eventToAdd);
        }


        public override string ToString()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("On ");
            strBuilder.Append(dayOfWeek);
            strBuilder.Append(", the ");
            strBuilder.Append(date.Day);
            strBuilder.Append(" of ");
            strBuilder.Append(date.TypeOfMonth());
            strBuilder.Append(" you have ");
            strBuilder.Append(eventsHandler.ToString());

            return strBuilder.ToString();


        }
    }
}
