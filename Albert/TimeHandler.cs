﻿using System;

namespace Albert
{
    internal class TimeHandler
    {
        private Time beginningTime;
        private Time endingTime;
        private DateTime beginningDate;
        private DateTime endingDate;

        public TimeHandler(Time beginningTime, Time endingTime, DateTime beginningDate) // If given only beginning date then assume it's on the same day
        {
            this.beginningTime = beginningTime;
            this.endingTime = endingTime;
            this.beginningDate = beginningDate;
            this.endingDate = beginningDate;
        }

        public TimeHandler(Time beginningTime, Time endingTime, DateTime beginningDate, DateTime endingDate)
        {
            this.beginningTime = beginningTime;
            this.endingTime = endingTime;
            this.beginningDate = beginningDate;
            this.endingDate = endingDate;
        }

        public Time getBeginningTime { get => beginningTime; }
        public Time getEndingTime { get => endingTime; }
        public DateTime getBeginningDate { get => beginningDate; }
        public DateTime getEndingDate { get => endingDate; }

        /*
        public bool Overlaps(TimeHandler other)
        {
            if(this.beginningDate.ComesBefore(other.beginningDate) && 
               this.endingDate.ComesBefore(other.endingDate) && 
               this.endingDate.ComesAfter(other.beginningDate))
        }
        */
    }
}