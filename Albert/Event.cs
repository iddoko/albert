﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class Event
    {
        private String typeOfEvent;
        private TimeHandler timeHandler;
        private String location;
        private Reminder reminder;

        public Event(DateTime date, Time beginningTime, Time endingTime, string location)
        {
            this.timeHandler = new TimeHandler(beginningTime, endingTime, date.Date);
            this.location = location;
            this.reminder = null;
        }

        public Event(DateTime date, Time beginningTime, Time endingTime, string location, Reminder reminder)
        {
            this.timeHandler = new TimeHandler(beginningTime, endingTime, date.Date);
            this.location = location;
            this.reminder = reminder;
        }

        public Event(DateTime beginningDate, DateTime endingDate, Time beginningTime, Time endingTime, string location, Reminder reminder)
        {
            this.timeHandler = new TimeHandler(beginningTime, endingTime, beginningDate.Date, endingDate.Date);
            this.location = location;
            this.reminder = reminder;
        }


        public Event(DateTime beginningDate, DateTime endingDate, Time beginningTime, Time endingTime, string location)
        {
            this.timeHandler = new TimeHandler(beginningTime, endingTime, beginningDate.Date, endingDate.Date);
            this.location = location;
            this.reminder = null;
        }

        public DayOfWeek getDay()
        {
            return this.timeHandler.getBeginningDate.DayOfWeek;
        }

        public DateTime getBeginningDate()
        {
            return this.timeHandler.getBeginningDate;
        }

        public override string ToString()
        {
            return ""
        }

    }
}
