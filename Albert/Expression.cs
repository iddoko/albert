﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    interface Expression
    {
        bool Interpret(String context);
    }
}
