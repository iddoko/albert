﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class TerminalExpression : Expression
    {
        private String data;
        public TerminalExpression(String data)
        {
            this.data = data;
        }

        public bool Interpret(String context)
        {
            return (context.Contains(data));
        }
    }
}
