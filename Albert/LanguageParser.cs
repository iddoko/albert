﻿using System;

namespace Albert
{
    internal class LanguageParser
    {
        private static LanguageParser instance = null;

        private TerminalExpression eventExpression;
        private TerminalExpression meetingExpression;
        private OrExpression setupExpression;
        private TerminalExpression nextExpression;
        private TerminalExpression tellmeExpression;
        private TerminalExpression weekExpression;
        private TerminalExpression tomorrowExpression;

        private AndExpression setupMeetingExpression;
        private AndExpression setupMeetingTomorrowExpression;
        private AndExpression setupEventExpression;
        private AndExpression setupEventTomorrowExpression;
        private AndExpression tellmeNextExpression;
        private AndExpression tellmeNextWeekExpression;
        private AndExpression tellmeTomorrow;

        public LanguageParser()
        {
            // Creates basic word interpretations
            eventExpression = new TerminalExpression("event");
            meetingExpression = new TerminalExpression("meeting");
            setupExpression = new OrExpression(new TerminalExpression("set up"),new TerminalExpression("setup"));
            nextExpression = new TerminalExpression("next");
            tellmeExpression = new TerminalExpression("tell me");
            weekExpression = new TerminalExpression("week");
            tomorrowExpression = new TerminalExpression("tomorrow");

            // Creates more complex combinational interpretations
            setupMeetingExpression = new AndExpression(setupExpression, meetingExpression);
            setupMeetingTomorrowExpression = new AndExpression(setupMeetingExpression, tomorrowExpression);
            setupEventExpression = new AndExpression(setupExpression, eventExpression);
            setupEventTomorrowExpression = new AndExpression(setupEventExpression, tomorrowExpression);
            tellmeNextExpression = new AndExpression(tellmeExpression, nextExpression);
            tellmeNextWeekExpression = new AndExpression(tellmeNextExpression, weekExpression);
            tellmeTomorrow = new AndExpression(tellmeExpression, tomorrowExpression);
        }


        public static LanguageParser getInstance()
        {
            if (instance == null) instance = new LanguageParser();
            return instance;
        }

        public void parseInput(String input)
        {
            if (setupEventTomorrowExpression.Interpret(input))
            {
                DateTime eventDate = new DateTime(2017, AlbertController.getInstance().Today().Month, AlbertController.getInstance().Today().Day + 1);
                Time beginningTime; // Have to find this somehow
                Time endingTime; // Have to find this somehow, but might end up not having it
                CalendarHandler.getInstance().addEvent(new Event(eventDate, beginningTime));
            }

            else if (setupMeetingTomorrowExpression.Interpret(input))
            {
                DateTime eventDate = new DateTime(2017, AlbertController.getInstance().Today().Month, AlbertController.getInstance().Today().Day + 1);
                Time beginningTime; // Have to find this somehow
                Time endingTime; // Have to find this somehow, but might end up not having it
                CalendarHandler.getInstance().addEvent(new Meeting(eventDate, beginningTime));
            }

            else if (setupEventExpression.Interpret(input))
            {
                DateTime eventDate; // Have to find this somehow
                Time beginningTime; // Have to find this somehow
                Time endingTime; // Have to find this somehow, but might end up not having it
                CalendarHandler.getInstance().addEvent(new Event(eventDate, beginningTime));
            }

            else if (setupMeetingExpression.Interpret(input))
            {
                DateTime eventDate; // Have to find this somehow
                Time beginningTime; // Have to find this somehow
                Time endingTime; // Have to find this somehow, but might end up not having it
                CalendarHandler.getInstance().addEvent(new Meeting(eventDate, beginningTime));
            }

            else if (tellmeTomorrow.Interpret(input))
            {
                Day tomorrow = CalendarHandler.getInstance().findDay(new DateTime(2017, AlbertController.getInstance().Today().Month, AlbertController.getInstance().Today().Day + 1));
                AlbertController.getInstance().Speak(tomorrow.ToString());
            }

            else if (tellmeNextWeekExpression.Interpret(input))
            {
                for()
                CalendarHandler.getInstance().findDay(new DateTime(2017, AlbertController.getInstance().Today().Month, AlbertController.getInstance().Today().Day + 1));
                AlbertController.getInstance().Speak(tomorrow.ToString());
            }
        }
    }
}