﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class CalendarHandler
    {
        private List<Day> May;
        private List<Day> June;
        private List<Day> July;
        private List<Day> August;
        private List<Day> September;
        private List<Day> October;
        private List<Day> November;
        private List<Day> December;

        private static CalendarHandler instance = null;

        private CalendarHandler()
        {
            CreateMonth(out May);
            CreateMonth(out June);
            CreateMonth(out July);
            CreateMonth(out August);
            CreateMonth(out September);
            CreateMonth(out October);
            CreateMonth(out November);
            CreateMonth(out December);
        }

        private static void CreateMonth(out List<Day> month)
        {
            month = new List<Day>();
            switch (nameof(month))
            {
                case "May":
                    for(int i = 0 ; i<31 ; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 5, i)));
                    }
                    break;

                case "June":
                    for (int i = 0; i < 30; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 6, i)));
                    }
                    break;

                case "July":
                    for (int i = 0; i < 31; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 7, i)));
                    }
                    break;

                case "August":
                    for (int i = 0; i < 31; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 8, i)));
                    }
                    break;

                case "September":
                    for (int i = 0; i < 30; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 9, i)));
                    }
                    break;

                case "October":
                    for (int i = 0; i < 31; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 10, i)));
                    }
                    break;

                case "November":
                    for (int i = 0; i < 30; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 11, i)));
                    }
                    break;

                case "December":
                    for (int i = 0; i < 31; i++)
                    {
                        month.Add(new Day(new DateTime(2017, 12, i)));
                    }
                    break;
            }
        }

        public static CalendarHandler getInstance()
        {
            if (instance == null) instance = new CalendarHandler();
            return instance;
        }

        public void addEvent(Event e) {
            switch (e.getBeginningDate().Month)
            {
                case 5:
                    May[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 6:
                    June[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 7:
                    July[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 8:
                    August[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 9:
                    September[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 10:
                    October[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 11:
                    November[e.getBeginningDate().Day].addEvent(e);
                    break;
                case 12:
                    December[e.getBeginningDate().Day].addEvent(e);
                    break;

            }
        }

        public Day findDay(DateTime date)
        {
            switch (date.Month)
            {
                case 5:
                    return May[date.Day];
                case 6:
                    return June[date.Day];
                case 7:
                    return July[date.Day];
                case 8:
                    return August[date.Day];
                case 9:
                    return September[date.Day];
                case 10:
                    return October[date.Day];
                case 11:
                    return November[date.Day];
                case 12:
                    return December[date.Day];
            }
            return null;
        }
    }
}
