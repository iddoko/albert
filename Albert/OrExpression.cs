﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class OrExpression : Expression
    {
        private Expression e1;
        private Expression e2;

        public OrExpression(Expression e1, Expression e2)
        {
            this.e1 = e1;
            this.e2 = e2;
        }

        public bool Interpret(string context)
        {
            return e1.Interpret(context) || e2.Interpret(context);
        }
    }
}
