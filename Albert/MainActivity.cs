﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class MainActivity
    {
        public static void main(string[] args) {
            // This is the main activity class for the albert personal assistant

            bool flag = true;
            const String startProgram = "Hey Albert";

            // Listens for incoming messages
            // Need to combine this with speech to text but for now...

            do
            { // Not good code, need to change this

                String input = SpeechRecognizer.getInput(); // Supposed to get this from the speech synthesizer

                if (startProgram.CompareTo(input) == 0) AlbertController.getInstance().startAlbert();

            } while(flag);
        }
    }
}
