﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    enum EventType
    {
        GENERIC_EVENT,
        MEETING,
        TASK,
        WORKOUT
    }
}
