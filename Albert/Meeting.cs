﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albert
{
    class Meeting : Event
    {
        private string person;

        public Meeting(DateTime date, Time beginningTime, Time endingTime, string location, string person, Reminder reminder) : base(date, beginningTime, endingTime, location, reminder)
        {
            this.person = person;
        }

        public Meeting(DateTime date, Time beginningTime, Time endingTime, string location, string person) : base(date, beginningTime, endingTime, location)
        {
            this.person = person;
        }
    }
}
