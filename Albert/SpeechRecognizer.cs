﻿using System;
using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace Albert
{
    internal class SpeechRecognizer
    {
        private SpeechRecognizer instance;

        private SpeechSynthesizer sSynth;
        private PromptBuilder pBuilder;
        private SpeechRecognitionEngine sRecognize;

        private SpeechRecognizer()
        {
            sSynth = new SpeechSynthesizer();
            pBuilder = new PromptBuilder();
            sRecognize = new SpeechRecognitionEngine();
        }

        public SpeechRecognizer getInstance()
        {
            if (this.instance == null) instance = new SpeechRecognizer();
            return instance;
        }

        public string getInput()
        {

        }

        public Speak(String str)
        {
            pBuilder.ClearContent();
            pBuilder.AppendText(str);
            sSynth.Speak(pBuilder);
        }

    }
}