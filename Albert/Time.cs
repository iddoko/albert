﻿namespace Albert
{
    public class Time
    {
        private int hours; // Ranges 0 - 24
        private int minutes; // Ranges 0 - 59


        public Time(int hours, int minutes)
        {
            if (hours >= 0 && hours <= 24) this.hours = hours;
            else throw new System.Exception();
            if(minutes >= 0 && minutes <= 59) this.minutes = minutes;
            else throw new System.Exception();
        }

        public int getHours { get => hours; }
        public int getMinutes { get => minutes; }

        public override string ToString()
        {
            string timeStr = "";
            if (minutes < 10) timeStr = hours + ":0" + minutes;
            else timeStr = hours + ":" + minutes;

            return timeStr;
        }

    }
}